%dw 2.0
output application/java
fun getNumSeats(planeType: String) = do {
    var maxSeats =
    if (planeType contains('737'))
    150
else
    300
    ---
    maxSeats
}
---
getNumSeats(payload.planeType)