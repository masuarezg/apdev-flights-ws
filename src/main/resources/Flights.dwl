%dw 2.0
output application/xml
/*var numSeats = (planeType: String) ->
  if (planeType contains ('737'))
    150
else
    300

fun getNumSeats(planeType: String) =
    if (planeType contains('737'))
    150
else
    300
*/
import dasherize from dw::core::Strings
type Currency = String {format: "###.00"}
type Flight = Object {class: "com.mulesoft.training.Flight"}

fun getNumSeats(planeType: String) = do {
    var maxSeats =
    if (planeType contains('737'))
    150
else
    300
    ---
    maxSeats
}
---
flights: (payload..*return map (object, index) -> {
	destination: object.destination,
//    price: object.price as Number,
//    price: object.price as Number {class: "java.lang.Double"},
    price: object.price as Number as Currency,
//    totalseats: getNumSeats(object.planeType as String),
    totalSeats: lookup("getTotalSeats",{planeType: object.planeType}),
//    planeType: upper(replace(object.planeType,/(Boing)/) with "Boeing"),
    planeType: dasherize(replace(object.planeType,/(Boing)/) with "Boeing"),
    departureDate: upper(object.departureDate as String),
//    date: object.departureDate as Date {format: "yyyy/MM/dd"}
//    fecha: object.departureDate as Date {format: "yyyy/MM/dd"} 
//           as String {format: "MM/yyyy/dd"}
//    code: lower(object.code)
      availableSeats: object.emptySeats as Number
} as Flight ) /* orderBy $.departureDate  orderBy $.price8 
             distinctBy$ orderBy $.departureDate
             orderBy $.price */
             distinctBy $filter ($.availableSeats !=0) orderBy $.departureDate 
             orderBy $.price